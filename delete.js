const { Router } = require('express');
const crypto = require('crypto');
let createError = require('http-errors');
let db = require('./model/db');

const router = Router();

router.delete('/user/:id', (req, res, next) => {
    db.deleteById(req.params.id)
        .then((count) => {
            if (count < 1) {
                next(createError(422));
            } else {
                res.sendStatus(204);
            }
        })
        .catch((e) => {
            console.error(e);
            next(createError(500));
        });
});

router.delete('/clearSessionMap', (req, res, next) => {
    let sessionMap = req.app.locals.sessionMap;
    let keysToDelete = [];
    let currentTime = new Date().getTime();
    for (let [key, value] of sessionMap) {
        if (value.expireTime <= currentTime) {
            keysToDelete.push(key);
        }
    }
    for (key of keysToDelete) {
        sessionMap.delete(key);
    }
});

module.exports = router;