const { Router } = require('express');
const crypto = require('crypto');
const db = require('./model/db');
const User = require('./model/user');
const ical = require('./model/ical');

const router = Router();

router.post('/register', async (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;
    var possibleUser = await db.getByUsername(username);
    if (possibleUser != null) {
        res.status(400);    // username je zauzet
        res.send("Username already taken.");
        res.end();
        return;
    }
    const userSalt = crypto.randomBytes(128).toString('base64');
    const hashedPassword = crypto.pbkdf2Sync(password, userSalt, 100000, 64, 'sha512').toString('hex');
    const calUrl = req.body.calUrl;
    if (username === "" || username == null || password === "" || password == null || calUrl === "" || calUrl == null) {
        res.status(400);
        res.end();
        return;
    } else {
        let user = new User(username, hashedPassword, userSalt, calUrl, new Date().toString(), 0);
        // insert user into database
        user = await db.insert(user);
        if (user == null){
            res.status(409);
            res.send("Error when inserting user into database.");
            res.end();
        } else {
            // dalje isto kao za login
            let userEvents = await ical.parseFromUrl(calUrl);
            let sessionID;
            do {
                sessionID = crypto.randomBytes(256).toString('base64');   // generiraj session ID
            } while(req.app.locals.sessionMap.has(sessionID));
            const expireTime = new Date(new Date().getTime() + parseInt(process.env.COOKIE_EXPIRE_TIME)).toUTCString(); // dodaj na trenutno vrijeme
            req.app.locals.sessionMap.set(sessionID, {expireTime: expireTime, events: userEvents});   // spremi u mapu
            const cookie = {
                sid: sessionID,
                expireTime: expireTime
            };
            res.send(cookie);
            res.end();
        }
    }
});

router.post('/login', async (req, res, next) => {
    let username = req.body.username;
    let password = req.body.password;
    const user = await db.getByUsername(username);
    if (user == null) {
        res.status(400);
        res.end();
        return;
    } else {
        const hashedPassword = crypto.pbkdf2Sync(password, user.passSalt, 100000, 64, 'sha512').toString('hex');
        const result = await db.validateUser(username, hashedPassword);
        if (result == null) {
            // login info incorrect
            res.status(400);
            res.send("Password incorrect.");
            res.end();
            return;
        } else {
            let userEvents = await ical.parseFromUrl(result.url)
            let sessionID;
            do {
                sessionID = crypto.randomBytes(256).toString('base64');   // generiraj session ID
            } while(req.app.locals.sessionMap.has(sessionID));
            const expireTime = new Date(new Date().getTime() + parseInt(process.env.COOKIE_EXPIRE_TIME)).toUTCString(); // dodaj na trenutno vrijeme
            req.app.locals.sessionMap.set(sessionID, {expireTime: expireTime, events: userEvents});   // spremi u mapu
            const cookie = {
                sid: sessionID,
                expireTime: expireTime
            };
            res.send(cookie);
            res.end();
        }
    }
});

router.post('/checkSession', (req, res, next) => {
    const sessionID = getCookie(req.headers.cookie, "sid");
    if (req.app.locals.sessionMap.has(sessionID)) {
        const expireTime = new Date() + process.env.COOKIE_EXPIRE_TIME; // dodaj na trenutno vrijeme
        const userEvents = req.app.locals.sessionMap.get(sessionID).events;
        req.app.locals.sessionMap.set(sessionID, {expireTime: expireTime, events: userEvents});   // spremi u mapu
        const cookie = {
            sid: sessionID,
            expireTime: expireTime
        };
        res.send(cookie);
    } else {
        res.status(400);
    }
    res.end();
});

router.post('/logout', (req, res, next) => {
    const sessionID = getCookie(req.headers.cookie, "sid");
    req.app.locals.sessionMap.delete(sessionID);
    res.send("Logged out.");
    res.end();
});

router.post('/message', async (req, res, next) => {
    const nlp = req.app.locals.nlp;
    const sessionID = getCookie(req.headers.cookie, "sid");
    if (!req.app.locals.sessionMap.has(sessionID)) {
        res.status(401);
        res.send("You are not logged in.");
        res.end();
        return;
    }
    const userEvents = req.app.locals.sessionMap.get(sessionID).events;
    const clientMessage = req.body.message;
    let nlpResult = await nlp.process('en', clientMessage);
    var currentTime = new Date();
    // currentTime.setMonth(0); // testing date
    if (nlpResult.intent === "test.next") {
        var minDiffTime = null;
        var firstTest = null;
        for (var event of userEvents) {
            if (event.type === "test") {
                var timeDifference = event.startTime - currentTime; // in miliseconds
                if (timeDifference > 0) {
                    // event is after current time
                    if (timeDifference < minDiffTime || minDiffTime == null) {
                        minDiffTime = timeDifference;
                        firstTest = event;
                    }
                }
            }
        }
        if (firstTest == null) {
            res.send(`You don't have any tests left.`);
        } else {
            res.send(`Your next test is in ${firstTest.subject} and it starts at ${firstTest.startTime}, it will be held in ${firstTest.location}.`);
        }
    } else if (nlpResult.intent === "test.unfinishedCount") {
        var unfinishedCount = 0;
        for (var event of userEvents) {
            if (event.type === "test") {
                var timeDifference = event.startTime - currentTime; // in miliseconds
                if (timeDifference > 0) {
                    // event is after current time
                    unfinishedCount++;
                }
            }
        }
        res.send(`You have ${unfinishedCount} tests left.`);
    } else if (nlpResult.intent === "test.finishedCount") {
        var finishedCount = 0;
        for (var event of userEvents) {
            if (event.type === "test") {
                var timeDifference = event.startTime - currentTime; // in miliseconds
                if (timeDifference <= 0) {
                    // event is before current time
                    finishedCount++;
                }
            }
        }
        res.send(`You have finished ${finishedCount} tests.`);
    } else if (nlpResult.intent === "lecture.next") {
        var minDiffTime = null;
        var firstLecture = null;
        for (var event of userEvents) {
            if (event.type === "lecture") {
                var timeDifference = event.startTime - currentTime; // in miliseconds
                if (timeDifference > 0) {
                    // event is after current time
                    if (timeDifference < minDiffTime || minDiffTime == null) {
                        minDiffTime = timeDifference;
                        firstLecture = event;
                    }
                }
            }
        }
        if (firstLecture == null) {
            res.send(`You don't have any lectures left.`);
        } else {
            res.send(`Your next lecture is in ${firstLecture.subject} and it starts at ${firstLecture.startTime}, it will be held in ${firstLecture.location}.`);
        }
    } else if (nlpResult.intent === "lab.unfinishedCount") {
        var unfinishedCount = 0;
        for (var event of userEvents) {
            if (event.type === "lab") {
                var timeDifference = event.startTime - currentTime; // in miliseconds
                if (timeDifference > 0) {
                    // event is after current time
                    unfinishedCount++;
                }
            }
        }
        res.send(`You have ${unfinishedCount} laboratory exercises left.`);
    } else if (nlpResult.intent === "lab.next") {
        var minDiffTime = null;
        var firstLab = null;
        for (var event of userEvents) {
            if (event.type === "lab") {
                var timeDifference = event.startTime - currentTime; // in miliseconds
                if (timeDifference > 0) {
                    // event is after current time
                    if (timeDifference < minDiffTime || minDiffTime == null) {
                        minDiffTime = timeDifference;
                        firstLab = event;
                    }
                }
            }
        }
        if (firstLab == null) {
            res.send(`You don't have any laboratory exercises left.`);
        } else {
            res.send(`Your next lab exercise is in ${firstLab.subject} and it starts at ${firstLab.startTime}, it will be held in ${firstLab.location}.`);
        }
    } else if (nlpResult.intent === "summary.thisWeek") {
        var eventCount = {};
        eventCount["lab"] = 0;
        eventCount["test"] = 0;
        eventCount["lecture"] = 0;
        let daysFromMonday = ((parseInt(currentTime.getDay()) + 6) % 7);
        var lastMonday = new Date(parseInt(currentTime.getTime()) - (daysFromMonday * 24 * 60 * 60 * 1000));
        lastMonday.setHours(0, 0, 0);
        var nextMonday = new Date(parseInt(lastMonday.getTime()) + (7 * 24 * 60 * 60 * 1000));
        for (var event of userEvents) {
            if (event.startTime.getTime() >= lastMonday.getTime() && event.startTime.getTime() < nextMonday.getTime()) {
                // event is in current week
                eventCount[event.type] = eventCount[event.type] + 1;
            }
        }
        res.send(`You have ${eventCount["lecture"]} lectures, ${eventCount["test"]} tests and ${eventCount["lab"]} lab exercises this week.`);
    } else if (nlpResult.intent === "summary.nextWeek") {
        var eventCount = {};
        eventCount["lab"] = 0;
        eventCount["test"] = 0;
        eventCount["lecture"] = 0;
        let daysFromMonday = ((parseInt(currentTime.getDay()) + 6) % 7);
        var lastMonday = new Date(parseInt(currentTime.getTime()) - (daysFromMonday * 24 * 60 * 60 * 1000));
        lastMonday.setHours(0, 0, 0);
        var nextMonday = new Date(parseInt(lastMonday.getTime()) + (7 * 24 * 60 * 60 * 1000));
        var nextMonday2 = new Date(parseInt(lastMonday.getTime()) + (14 * 24 * 60 * 60 * 1000));
        for (var event of userEvents) {
            if (event.startTime.getTime() >= nextMonday.getTime() && event.startTime.getTime() < nextMonday2.getTime()) {
                // event is in current week
                eventCount[event.type] = eventCount[event.type] + 1;
            }
        }
        res.send(`You have ${eventCount["lecture"]} lectures, ${eventCount["test"]} tests and ${eventCount["lab"]} lab exercises next week.`);
    } else if (nlpResult.intent === "None") {
        res.send(`Sorry, I didn't quite understand that. Could you please try saying it in a different way?`);
    }
    res.end();
});

module.exports = router;

function getCookie(allCookiesString, cookieName) {
    try {
        let cookies = allCookiesString.split(";");
        for (let cookieString of cookies) {
            cookieString = cookieString.trim();
            if (cookieString.startsWith(cookieName + "=")) {
                return cookieString.substring(cookieName.length + 1);
            }
        }
    } catch(e) {
        return "";
    }
    return "";
}
