#!/bin/bash

docker-compose up -d && echo "Now press Ctrl+C"
sed '/^DB_HOST/d' && echo "DB_HOST=$(docker exec -itd ppij_db hostname -I)" > .env