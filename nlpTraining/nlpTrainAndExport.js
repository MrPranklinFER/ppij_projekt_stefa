const fs = require('fs');
const { NlpManager } = require('node-nlp');

var myNlp = new NlpManager({languages:['en']});

fs.readFile('./intentAnalysisTrainingData.json', {'encoding':'utf8'}, async function(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    const fileData = JSON.parse(data);  // gives an Array
    for (var intentObject of fileData) {
        for (var question of intentObject.questions)
        myNlp.addDocument('en', question, intentObject.intent);
    }

    await myNlp.train();
    const jsonData = myNlp.export(true);
    fs.writeFile('./savedNlpModel.json', jsonData, {'encoding':'utf8'}, () => {
        console.log("File export completed!");
    });
});