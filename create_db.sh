#!/bin/bash

# copy sql commands into container `ppij_db`
docker cp ./ppij_projekt.sql ppij_db:/ppij_projekt.sql && \
# execute them as inside container in `psql` (PostgreSQL CLI) as user (-U) `postgres` in db (-d) `postgres`
docker exec -it ppij_db psql -U postgres -d postgres -a -f ./ppij_projekt.sql