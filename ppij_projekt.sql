CREATE ROLE ppij_backend WITH PASSWORD 'ppij1111' LOGIN CREATEDB;

\connect postgres ppij_backend;
DROP DATABASE ppij_db;
CREATE DATABASE ppij_db;
\connect ppij_db;

CREATE TABLE "users" (
  "id" serial PRIMARY KEY,
  "username" varchar(255) UNIQUE NOT NULL,
  "pass_hash" varchar(255),
  "pass_salt" varchar(255),
  "cal_url" varchar(255) DEFAULT '',
  "created_at" timestamp DEFAULT (now())
);

CREATE OR REPLACE function validateUser(_username varchar, _pass_hash varchar)
    RETURNS TABLE (id_ int, username_ varchar, cal_url_ varchar, created_at_ timestamp ) as
$$
DECLARE
    query_result varchar;
begin
    SELECT pass_hash INTO query_result FROM users WHERE username = _username LIMIT 1;
    if (query_result = _pass_hash) then
        return QUERY SELECT id, username, cal_url, created_at FROM users WHERE username = _username LIMIT 1;
    else
        return;
    end if;
end
$$
LANGUAGE 'plpgsql';
