class User {
    constructor(username, passHash="", passSalt="", url, createdAt = undefined, id=0) {
        this.username = username;
        this.passHash = passHash;
        this.passSalt = passSalt;
        this.url = url;
        this.createdAt = createdAt;
        this.id = id;
    }
}

module.exports = User;