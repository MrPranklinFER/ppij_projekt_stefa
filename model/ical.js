const ical = require('node-ical');
const Event = require('./event');

/**
 * VEVENT describes an event, which has a scheduled amount of time on a calendar
 * @type {string}
 */
const TYPE_EVENT = 'VEVENT';

/**
 * @param {string} url
 * @returns {Promise<Event[]>}
 */
async function parseFromUrl (url) {
    let values = await ical.async.fromURL(url);

    return parseICS(values)
}

/**
 *
 * @param {string} filename
 * @returns {Promise<Event[]>}
 */
async function parseFromFile(filename){
    let values = await ical.async.parseFile(filename);

    return parseICS(values)
}

/**
 *
 * @param values - parsed by 'node-ical'
 * @returns {Promise<Event[]>}
 */
async function parseICS(values){
    let arr = Array(0);

    for (let key in values) {
        if (values.hasOwnProperty(key)) {
            let event = values[key];
            if (event.type === TYPE_EVENT) {
                if (event.summary.search("predavanje") !== -1) {
                    event.type = "lecture";
                } else if (event.summary.search("laboratorij") !== -1) {
                    event.type = "lab";
                } else if (event.summary.search("ispit") !== -1) {
                    event.type = "test";
                }
                let newEvent = new Event(
                    new Date(event.start),
                    new Date(event.end),
                    event.summary,
                    event.location,
                    event.summary.substring(0, event.summary.search(" - ")), // subject
                    event.uid,
                    event.type
                );
                arr.push(newEvent);
            }
        }
    }

    return arr;
}

module.exports = {
    parseFromUrl: parseFromUrl,
    parseFromFile: parseFromFile
};