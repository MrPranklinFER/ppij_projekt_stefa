const {Client} = require('pg');
const User = require('./user');

const client = new Client({
    host: process.env.DB_HOST,
    database: process.env.DB_DB,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD
})

module.exports.connect = async function () {
    return client.connect();
}

module.exports.close = async function () {
    return client.end();
}

module.exports.getUsers = async function () {
    return client.query('select * from users')
        .then((res) => {
            let users = new Array(0);
            res.rows.forEach((u) => {
                let user = new User(u.username, u.pass_hash, u.passSalt, u.cal_url, new Date(u.created_at), u.id);
                users.push(user);
            })
            return users;
        })
}

module.exports.getById = async function (id) {
    return client.query('select * from users where id = $1', [id])
        .then((res) => {
            if (res.rowCount > 0) {
                let row = res.rows[0];
                return new User(row.username, row.pass_hash, row.pass_salt, row.cal_url, row.created_at, row.id);
            } else {
                return null;
            }
        })
}

module.exports.getByUsername = async function (username) {
    return client.query('select * from users where username = $1', [username])
        .then((res) => {
            if (res.rowCount > 0) {
                let row = res.rows[0];
                return new User(row.username, row.pass_hash, row.pass_salt, row.cal_url, row.created_at, row.id);
            } else {
                return null;
            }
        })
}

module.exports.deleteById = async function (id) {
    return client.query('delete from users where id = $1 returning *', [id])
        .then((res) => {
            return res.rowCount;
        })
}

module.exports.insert = async function (user) {
    return client.query('insert into users (username, pass_hash, pass_salt, cal_url) values ($1, $2, $3, $4) returning *', [user.username, user.passHash, user.passSalt, user.url])
        .then((res) => {
            let row = res.rows[0];
            return new User(row.username, row.pass_hash, row.pass_salt, row.cal_url, row.created_at, row.id)
        })
        .catch((exc) => {
            if (exc.code === 23505) {
                console.error(exc);
                return null;
            }
        })
}

module.exports.validateUser = async function (username, passHash) {
    return client.query('select * from validateUser($1, $2)', [username, passHash])
        .then((res) => {
            if (res.rowCount < 1) {
                return null
            } else {
                let row = res.rows[0];
                return new User(row.username_, "", "", row.cal_url_, row.created_at_, row.id_);
            }
        })
}