class Event {
    /**
     *
     * @param {Date} startTime - timezone aware
     * @param {Date} endTime - timezone aware
     * @param {string} summary
     * @param {string} location
     * @param {string} subject
     * @param {string} uid
     * @param {string} type
     */
    constructor(startTime, endTime, summary, location, subject, uid, type) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.summary = summary;
        this.location = location;
        this.subject = subject;
        this.uid = uid;
        this.type = type;
    }
}

module.exports = Event;