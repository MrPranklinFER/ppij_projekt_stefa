FROM node:12.13.0
EXPOSE 3000 5432

WORKDIR /home/app

COPY package.json /home/app/
COPY package-lock.json /home/app/

RUN npm ci

COPY . /home/app

CMD ["npm", "start"]