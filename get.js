const { Router } = require('express');
let db = require('./model/db');
const User = require('./model/user');

const router = Router();

router.get('/allUsers', function (req, res, next) {
    db.getUsers()
        .then((users) => {
            res.send(users);
            res.end();
        })
        .catch((e) => {
            console.error(e);
            res.status(500);
            res.end();
        });
});

router.get('/user/:id', function (req, res, next) {
    if (!isNaN(parseInt(req.params.id))) {
        db.getById(req.params.id)
            .then((user) => {
                res.send(user);
                res.end();
            })
            .catch((e) => {
                console.error(e);
                res.status(500);
                res.end();
            });
    } else {
        res.status(400);
        res.send("ID must be a number.");
        res.end();
    }
});

module.exports = router;
