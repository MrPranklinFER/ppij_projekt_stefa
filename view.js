const { Router } = require('express');
const path = require('path');

const router = Router();

router.get('/', function (req, res, next) {
    const sessionID = getCookie(req.headers.cookie, "sid");
    if(req.app.locals.sessionMap.has(sessionID)) {
        res.sendFile(path.join(__dirname, '/view/index2.html'));
    } else {
        res.redirect('/login');
    }
});

router.get('/register', function (req, res, next) {
    const sessionID = getCookie(req.headers.cookie, "sid");
    if(req.app.locals.sessionMap.has(sessionID)) {
        res.redirect('/');
    } else {
        res.sendFile(path.join(__dirname, '/view/register.html'));
    }
});

router.get('/login', function (req, res, next) {
    const sessionID = getCookie(req.headers.cookie, "sid");
    if(req.app.locals.sessionMap.has(sessionID)) {
        res.redirect('/');
    } else {
        res.sendFile(path.join(__dirname, '/view/login.html'));
    }
});

module.exports = router;

function getCookie(allCookiesString, cookieName) {
    try {
        let cookies = allCookiesString.split(";");
        for (let cookieString of cookies) {
            cookieString = cookieString.trim();
            if (cookieString.startsWith(cookieName + "=")) {
                return cookieString.substring(cookieName.length + 1);
            }
        }
    } catch(e) {
        return "";
    }
    return "";
}
