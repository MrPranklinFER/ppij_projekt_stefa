# PPIJ_Projekt_Stefa

## How to use the database

1. start the database container using `start_db.sh` - this
   will update (set if not present) DB_HOST in `.env`
   - the default way to start a container by using
     `docker-compose up`, but then you will have to manually
     set DB_HOST in `.env` to the container IP address

2. if you are running it for the first time, run
   `create_db.sh` - it will create role, database used by
   the app and `users` table with test data

3. to stop the container run `docker-compose stop`

## API docs

[Postman collection](https://documenter.getpostman.com/view/8914171/SzmfXH3p?version=latest)