document.getElementById("message_button").addEventListener("click", (e) => {
    var message = document.getElementById("msg").value;
    if(message) {
        var req = new XMLHttpRequest();
        var url = "/post/message";
        var textarea = document.getElementById("chatbox");
        var params = {'message':message}
        
        document.getElementById("msg").value = "";
        textarea.value += "You: "+message+"\n";
        textarea.scrollTop = textarea.scrollHeight;
        req.onreadystatechange = function() {
            
            if (this.status === 200 && this.readyState === 4) {
                // request is finished and completed succesfully
                var result = req.responseText;
                textarea.value += "Bot: "+result+"\n";
                textarea.scrollTop = textarea.scrollHeight;
            } else if (this.readyState === 4) {
                // request finished but failed
                textarea.value += "Error";
            }
        }
        
        
        req.open('POST', url, true);
        req.setRequestHeader('Content-Type', "application/json; charset=UTF-8");
        req.send(JSON.stringify(params));
    }
});


document.getElementById("msg").addEventListener("keyup", function(event) {
    if(event.keyCode === 13) {
        document.getElementById("message_button").click();
    }
});

document.getElementById("logout_button").addEventListener("click", (e) => {
    
    var req = new XMLHttpRequest();
    var url = "/post/logout";

    req.onreadystatechange = function() {
        
        if (this.status === 200 && this.readyState === 4) {
            // request is finished and completed succesfully
            location.reload(true);
        } else if (this.readyState === 4) {
            // request finished but failed
        }
    }

    req.open('POST', url, true);
    req.setRequestHeader('Content-Type', "application/json; charset=UTF-8");
    req.send();
    
});