document.getElementById("login_button").addEventListener("click", (e) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            // request is finished and completed succesfully
            var result = JSON.parse(req.responseText);
            var date = new Date(result.expireTime);
            date = date.toUTCString();
            console.log(date);
            var div = document.getElementById("result");
            var ck = `sid=${result.sid}; expires=${date}`;
            document.cookie = ck;
            div.innerHTML = '<div class="alert alert-success" role="alert">' +
                                'Success' +
                            '</div>';
            location.reload(true);
        } else if (this.readyState === 4 && this.status === 400) {
            // request finished but failed
            var div = document.getElementById("result");
            div.innerHTML = '<div class="alert alert-danger" role="alert">' +
                                'Failed:(Password incorrect?)' +
                            '</div>';
        } else if (this.readyState === 4) {
            // request finished but failed
            var div = document.getElementById("result");
            div.innerHTML = '<div class="alert alert-danger" role="alert">' +
                                'Request failed:(' +
                            '</div>';
        }
    }
    
    var url = "/post/login";
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var params = {'username':username, 'password':password}



    req.open('POST', url, true);
    req.setRequestHeader('Content-Type', "application/json; charset=UTF-8");
    req.send(JSON.stringify(params));
});