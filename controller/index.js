document.getElementById("send_button").addEventListener("click", (e) => {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            // request is finished and completed succesfully
            var result = JSON.stringify(req.responseText);
            document.getElementById("request_result").value = result;
        } else if (this.readyState === 4) {
            // request finished but failed
            document.getElementById("request_result").value = "ERROR " + this.status;
        }
    }
    var select_element = document.getElementById("request_type");
    var request_type = select_element.options[select_element.selectedIndex].value;
    var url = document.getElementById("addres_input").value;
    url = "http://localhost:3000/" + request_type.toLowerCase() + "/" + url;
    var params = JSON.parse(document.getElementById("request_params").value);

    req.open(request_type, url, true);
    req.setRequestHeader('Content-Type', "application/json; charset=UTF-8");
    req.send(JSON.stringify(params));
});