require('dotenv').config();
const bodyParser = require('body-parser');
const express = require('express');
const createError = require('http-errors');
const logger = require('morgan');
const fs = require('fs');
const {NlpManager} = require('node-nlp');

const db = require('./model/db');
const ical = require('./model/ical');

const app = express();

// Create NLP manager
var nlpManager = new NlpManager({languages: ['en']});
fs.readFile('./nlpTraining/savedNlpModel.json', {encoding: 'utf-8'}, async (error, trainedNlpData) => {
    nlpManager.import(trainedNlpData);  // import trained data for nlp
    await nlpManager.train();
    console.log("NLP successfully trained.");
    app.locals.nlp = nlpManager;    // save nlp to app parameters
});

// body parsing middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(logger('dev'));
// Routing - middleware
app.use('/post', require('./post.js'));
app.use('/get', require('./get.js'));
app.use('/controller', express.static('controller'));
app.use('/style', express.static('style'));
app.use('/img', express.static('img'));
app.use(require('./view.js'));

// catching 404 and forwarding it
app.use(function (req, res, next) {
    //next(createError(404));
});

// open session map
app.locals.sessionMap = new Map();

// load ical
ical.parseFromFile('./mycal-example.ics')
    .then((events) => {
            app.locals.events = events;
        }
    ).catch((e) => console.error(e));

// connect to database
db.connect()
    .then(() => {
        console.log("Connected to database.");
    })
    .catch((e) => console.error(e));

// open server
app.listen(process.env.APP_PORT);